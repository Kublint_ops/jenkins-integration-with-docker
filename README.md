**What is CICD ?**


Continuous Integration and Continuous Deployment (CI/CD) is an essential aspect of software development. Developers use tools like Jenkins to automate the process of building, testing, and deploying code to production. However, setting up Jenkins can be a complex task. Docker provides an efficient way to deploy applications and their dependencies, and it can also be used to run Jenkins. In this blog, we will explore how to integrate Jenkins with Docker.

![Jenkins with docker](./jenkins.png)

**Prerequisites:**

Docker installed on your machine
Basic knowledge of Docker and Jenkins

**Step 1: Installing Jenkins**

You can download and install Jenkins from the official website. Once Jenkins is installed, start the service using the following command:

`sudo systemctl start jenkins`

By default, Jenkins runs on port 8080. To access Jenkins, open a web browser and enter http://localhost:8080/.

**Step 2: Installing Docker Plugin**

The Docker plugin for Jenkins allows you to build, publish, and run Docker containers from within Jenkins. To install the plugin, go to the Jenkins dashboard, click on "Manage Jenkins" and then "Manage Plugins." In the "Available" tab, search for "Docker" and select the "Docker" plugin. Click on "Install without restart" to install the plugin.

**Step 3: Configuring Jenkins to use Docker**

To use Docker with Jenkins, you need to configure Jenkins to use the Docker daemon. To do this, go to "Manage Jenkins" > "Configure System" > "Cloud" > "Docker" and enter the Docker Host URL, which is typically unix:///var/run/docker.sock.

**Step 4: Creating a Jenkins Pipeline with Docker**


To create a Jenkins pipeline with Docker, you need to define the steps in a Jenkinsfile. Here is an example of a Jenkinsfile that builds a Docker image, pushes it to Docker Hub, and runs it.

```
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'docker build -t yourdockerusername/yourappname .'
            }
        }
        stage('Push') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-hub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'docker login -u $USERNAME -p $PASSWORD'
                }
                sh 'docker push yourdockerusername/yourappname'
            }
        }
        stage('Deploy') {
            steps {
                sh 'docker run -p 8080:8080 yourdockerusername/yourappname'
            }
        }
    }
}
```



This Jenkinsfile contains three stages: Build, Push, and Deploy. In the Build stage, the Docker image is built using the docker build command. In the Push stage, the Docker image is pushed to Docker Hub using the docker login and docker push commands. In the Deploy stage, the Docker image is run using the docker run command.

Note that in the Push stage, we use the withCredentials block to securely pass the Docker Hub username and password to Jenkins.

**Step 5: Running the Pipeline**

To run the pipeline, click on "New Item" in the Jenkins dashboard, enter a name for the pipeline, and select "Pipeline" as the project type. In the "Pipeline" section, select "Pipeline script from SCM" and enter the URL of your Git repository containing the Jenkinsfile.

Save the pipeline and click on "Build Now" to run it. Jenkins will build the Docker image, push it to Docker Hub, and then run it.

**Conclusion:**

Integrating Jenkins with Docker provides a powerful way to automate the process of building, testing, and deploying applications. With Docker, you can easily manage

Author - [SomayMangla](https://www.linkedin.com/in/er-somay-mangla/)


#jenkins #docker #cicd #devops #somaymangla #kublintops #ansible #aws 
